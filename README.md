# Author
Andri Muhyidin

# Dockerfile
## Container
Creating image for deploying in kubernetes cluster, it's only sample image for reactjs application (web-apps).
This is my personal image in directory web-apps, so I build based on my repository name, you can change it based on docker hub ID

Build
$docker build -t andrimuhyidin/react-apps:latest .

Push
$docker push andrimuhyidin/react-apps:latest

Pull
$docker pull andrimuhyidin/react-apps:latest

## Helm Package
My helm package manager reference from my image in andrimuhyidin/react-apps:latest with default requirement:
a. All configuration either in environment or ConfigMap
b. CPU request/limit : 50m/150m
c. Memory request/limit : 80MB/128MB
d. Node Affinity:
- app-deployment = true
- db-deployment = false
- monitoring-deployment = false
e. App update strategy: Rolling update
f. Specify HPA/HA and PDB for the pod
- HA/HPA : min/max 5/10
- PDB: 2
g. Enable and specify health-check.

Deploy the helm in your cluster
$helm install web-apps web-apps -n namespace

or update your installation if have some changes
$helm upgrade web-apps web-apps -n namespace

## Sample Result
![alt text](img/result.png)